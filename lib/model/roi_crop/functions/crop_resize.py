# functions/add.py
import torch
from torch.autograd import Function
from .._ext import roi_crop
from cffi import FFI
ffi = FFI()

class RoICropFunction(Function):
    def forward(self, input1, input2):
        self.input1 = input1
        self.input2 = input2
        self.device_c = ffi.new("int *")
        output = torch.zeros(input2.size()[0], input1.size()[1], input2.size()[1], input2.size()[2])
        #print('decice %d' % torch.cuda.current_device())
        if input1.is_cuda:
            self.device = torch.cuda.current_device()
        else:
            self.device = -1
        self.device_c[0] = self.device
        if not input1.is_cuda:
            roi_crop.BilinearSamplerBHWD_updateOutput(input1, input2, output)
        else:
            output = output.cuda(self.device)
            roi_crop.BilinearSamplerBHWD_updateOutput_cuda(input1, input2, output)
        return output

    def backward(self, grad_output):
        grad_input1 = torch.zeros(self.input1.size())
        grad_input2 = torch.zeros(self.input2.size())
        #print('backward decice %d' % self.device)
        if not grad_output.is_cuda:
            roi_crop.BilinearSamplerBHWD_updateGradInput(self.input1, self.input2, grad_input1, grad_input2, grad_output)
        else:
            grad_input1 = grad_input1.cuda(self.device)
            grad_input2 = grad_input2.cuda(self.device)
            roi_crop.BilinearSamplerBHWD_updateGradInput_cuda(self.input1, self.input2, grad_input1, grad_input2, grad_output)
        return grad_input1, grad_input2

# import math
# import torch
# import torch.nn as nn
# import torch.nn.functional as F
# from torch.autograd import Function

# from ._ext import crop_and_resize as _backend


# class CropAndResizeFunction(Function):

#     def __init__(self, crop_height, crop_width, extrapolation_value=0):
#         self.crop_height = crop_height
#         self.crop_width = crop_width
#         self.extrapolation_value = extrapolation_value

#     def forward(self, image, boxes, box_ind):
#         crops = torch.zeros_like(image)

#         if image.is_cuda:
#             _backend.crop_and_resize_gpu_forward(
#                 image, boxes, box_ind,
#                 self.extrapolation_value, self.crop_height, self.crop_width, crops)
#         else:
#             _backend.crop_and_resize_forward(
#                 image, boxes, box_ind,
#                 self.extrapolation_value, self.crop_height, self.crop_width, crops)

#         # save for backward
#         self.im_size = image.size()
#         self.save_for_backward(boxes, box_ind)

#         return crops

#     def backward(self, grad_outputs):
#         boxes, box_ind = self.saved_tensors

#         grad_outputs = grad_outputs.contiguous()
#         grad_image = torch.zeros_like(grad_outputs).resize_(*self.im_size)

#         if grad_outputs.is_cuda:
#             _backend.crop_and_resize_gpu_backward(
#                 grad_outputs, boxes, box_ind, grad_image
#             )
#         else:
#             _backend.crop_and_resize_backward(
#                 grad_outputs, boxes, box_ind, grad_image
#             )

#         return grad_image, None, None


# class CropAndResize(nn.Module):
#     """
#     Crop and resize ported from tensorflow
#     See more details on https://www.tensorflow.org/api_docs/python/tf/image/crop_and_resize
#     """

#     def __init__(self, crop_height, crop_width, extrapolation_value=0):
#         super(CropAndResize, self).__init__()

#         self.crop_height = crop_height
#         self.crop_width = crop_width
#         self.extrapolation_value = extrapolation_value

#     def forward(self, image, boxes, box_ind):
#         return CropAndResizeFunction(self.crop_height, self.crop_width, self.extrapolation_value)(image, boxes, box_ind)