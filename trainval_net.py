# --------------------------------------------------------
# Pytorch multi-GPU Faster R-CNN
# Licensed under The MIT License [see LICENSE for details]
# Written by Jiasen Lu, Jianwei Yang, based on code from Ross Girshick
# --------------------------------------------------------
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import _init_paths
import logging
import random
import os
import sys
import numpy as np
import argparse
import pprint
import pdb
import time

import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.optim as optim

import torchvision.transforms as transforms
from torch.utils.data.sampler import Sampler

from roi_data_layer.roidb import combined_roidb
from roi_data_layer.roibatchLoader import roibatchLoader
from model.utils.config import cfg, cfg_from_file, cfg_from_list, get_output_dir
from model.utils.net_utils import weights_normal_init, save_net, load_net, \
	adjust_learning_rate, save_checkpoint, clip_gradient

from model.faster_rcnn.vgg16 import vgg16
from coco_loader import CocoDataset, data_generator, CocoConfig

from torchsummary import summary


import os
import time
import numpy as np

# Download and install the Python COCO tools from https://github.com/waleedka/coco
# That's a fork from the original https://github.com/pdollar/coco with a bug
# fix for Python 3.
# I submitted a pull request https://github.com/cocodataset/cocoapi/pull/50
# If the PR is merged then use the original repo.
# Note: Edit PythonAPI/Makefile and replace "python" with "python3".
from pycocotools.coco import COCO
from pycocotools.cocoeval import COCOeval
from pycocotools import mask as maskUtils

import zipfile
import urllib.request
import shutil




def parse_args():
	"""
	Parse input arguments
	"""
	parser = argparse.ArgumentParser(description='Train a Fast R-CNN network')
	parser.add_argument('--dataset', dest='dataset',
						help='training dataset',
						default='coc', type=str)
	parser.add_argument('--dataset-dir', dest='dataset_dir',
						help='training dataset',
						default='/media/DataDriveA/Datasets/COCO/', type=str)
	parser.add_argument('--pre-trained-path', dest='pre_trained_path',
						help='pre trained model path',
						default='faster_rcnn_1_19_48611.pth', type=str)
	parser.add_argument('--net', dest='net',
						help='vgg16, res101',
						default='vgg16', type=str)
	parser.add_argument('--start_epoch', dest='start_epoch',
						help='starting epoch',
						default=1, type=int)
	parser.add_argument('--epochs', dest='max_epochs',
						help='number of epochs to train',
						default=20, type=int)
	parser.add_argument('--disp_interval', dest='disp_interval',
						help='number of iterations to display',
						default=100, type=int)
	parser.add_argument('--checkpoint_interval', dest='checkpoint_interval',
						help='number of iterations to display',
						default=10000, type=int)

	parser.add_argument('--save_dir', dest='save_dir',
						help='directory to save models', default="saved_models",
						nargs=argparse.REMAINDER)
	parser.add_argument('--nw', dest='num_workers',
						help='number of worker to load data',
						default=0, type=int)
	parser.add_argument('--cuda', dest='cuda',
						help='whether use CUDA',
						action='store_true')
	parser.add_argument('--ls', dest='large_scale',
						help='whether use large imag scale',
						action='store_true')
	parser.add_argument('--mGPUs', dest='mGPUs',
						help='whether use multiple GPUs',
						action='store_true')
	parser.add_argument('--bs', dest='batch_size',
						help='batch_size',
						default=1, type=int)
	parser.add_argument('--cag', dest='class_agnostic',
						help='whether perform class_agnostic bbox regression',
						action='store_true')

	# config optimization
	parser.add_argument('--o', dest='optimizer',
						help='training optimizer',
						default="sgd", type=str)
	parser.add_argument('--lr', dest='lr',
						help='starting learning rate',
						default=0.001, type=float)
	parser.add_argument('--lr_decay_step', dest='lr_decay_step',
						help='step to do learning rate decay, unit is epoch',
						default=5, type=int)
	parser.add_argument('--lr_decay_gamma', dest='lr_decay_gamma',
						help='learning rate decay ratio',
						default=0.1, type=float)

	# set training session
	parser.add_argument('--s', dest='session',
						help='training session',
						default=1, type=int)

	# resume trained model
	parser.add_argument('--r', dest='resume',
						help='resume checkpoint or not',
						default=False, type=bool)
	parser.add_argument('--checksession', dest='checksession',
						help='checksession to load model',
						default=1, type=int)
	parser.add_argument('--checkepoch', dest='checkepoch',
						help='checkepoch to load model',
						default=1, type=int)
	parser.add_argument('--checkpoint', dest='checkpoint',
						help='checkpoint to load model',
						default=0, type=int)
	# log and diaplay
	parser.add_argument('--use_tfboard', dest='use_tfboard',
						help='whether use tensorflow tensorboard',
						default=False, type=bool)

	# misc
	parser.add_argument('--year', dest='year',
						help='year of data',
						default='2014', type=str)
	parser.add_argument('--download', dest='download',
						help='auto download',
						default=True, type=bool)

	args = parser.parse_args()
	return args


class sampler(Sampler):
	def __init__(self, train_size, batch_size):
		self.num_data = train_size
		self.num_per_batch = int(train_size / batch_size)
		self.batch_size = batch_size
		self.range = torch.arange(0, batch_size).view(1, batch_size).long()
		self.leftover_flag = False
		if train_size % batch_size:
			self.leftover = torch.arange(self.num_per_batch * batch_size, train_size).long()
			self.leftover_flag = True

	def __iter__(self):
		rand_num = torch.randperm(self.num_per_batch).view(-1, 1) * self.batch_size
		self.rand_num = rand_num.expand(self.num_per_batch, self.batch_size) + self.range

		self.rand_num_view = self.rand_num.view(-1)

		if self.leftover_flag:
			self.rand_num_view = torch.cat((self.rand_num_view, self.leftover), 0)

		return iter(self.rand_num_view)

	def __len__(self):
		return self.num_data


if __name__ == '__main__':

	args = parse_args()

	print('Called with args:')
	print(args)

	if args.use_tfboard:
		from model.utils.logger import Logger

		# Set the logger
		logger = Logger('./logs')

	if args.dataset == "pascal_voc":
		args.imdb_name = "voc_2007_trainval"
		args.imdbval_name = "voc_2007_test"
		args.set_cfgs = ['ANCHOR_SCALES', '[8, 16, 32]', 'ANCHOR_RATIOS', '[0.5,1,2]', 'MAX_NUM_GT_BOXES', '20']
	elif args.dataset == "pascal_voc_0712":
		args.imdb_name = "voc_2007_trainval+voc_2012_trainval"
		args.imdbval_name = "voc_2007_test"
		args.set_cfgs = ['ANCHOR_SCALES', '[8, 16, 32]', 'ANCHOR_RATIOS', '[0.5,1,2]', 'MAX_NUM_GT_BOXES', '20']
	elif args.dataset == "coco":
		args.imdb_name = "coco_2014_train+coco_2014_valminusminival"
		args.imdbval_name = "coco_2014_minival"
		args.set_cfgs = ['ANCHOR_SCALES', '[4, 8, 16, 32]', 'ANCHOR_RATIOS', '[0.5,1,2]', 'MAX_NUM_GT_BOXES', '50']
	elif args.dataset == "imagenet":
		args.imdb_name = "imagenet_train"
		args.imdbval_name = "imagenet_val"
		args.set_cfgs = ['ANCHOR_SCALES', '[4, 8, 16, 32]', 'ANCHOR_RATIOS', '[0.5,1,2]', 'MAX_NUM_GT_BOXES', '30']
	elif args.dataset == "vg":
		# train sizes: train, smalltrain, minitrain
		# train scale: ['150-50-20', '150-50-50', '500-150-80', '750-250-150', '1750-700-450', '1600-400-20']
		args.imdb_name = "vg_150-50-50_minitrain"
		args.imdbval_name = "vg_150-50-50_minival"
		args.set_cfgs = ['ANCHOR_SCALES', '[4, 8, 16, 32]', 'ANCHOR_RATIOS', '[0.5,1,2]', 'MAX_NUM_GT_BOXES', '50']

	args.cfg_file = "cfgs/{}_ls.yml".format(args.net) if args.large_scale else "cfgs/{}.yml".format(args.net)

	# if args.cfg_file is not None:
	#     cfg_from_file(args.cfg_file)
	# if args.set_cfgs is not None:
	#     cfg_from_list(args.set_cfgs)

	pre_trained_model_path = args.pre_trained_path
	# print('Using config:')
	# pprint.pprint(cfg)
	# np.random.seed(cfg.RNG_SEED)

	# torch.backends.cudnn.benchmark = True
	if torch.cuda.is_available() and not args.cuda:
		print("WARNING: You have a CUDA device, so you should probably run with --cuda")

	# # train set
	# # -- Note: Use validation set and disable the flipped to enable faster loading.
	# cfg.TRAIN.USE_FLIPPED = True
	# cfg.USE_GPU_NMS = args.cuda
	# TODO: Can be replaced by single RoiDb i.e COCO and fixed ratio list and index
	# imdb, roidb, ratio_list, ratio_index = combined_roidb(args.imdb_name)
	# train_size = len(roidb)

	# print('{:d} roidb entries'.format(len(roidb)))

	output_dir = args.save_dir + "/" + args.net + "/" + args.dataset
	if not os.path.exists(output_dir):
		os.makedirs(output_dir)

	# sampler_batch = sampler(train_size, args.batch_size)

	# TODO Replace the current dataloader with COCO.
	# dataset = roibatchLoader(roidb, ratio_list, ratio_index, args.batch_size, \
	#                         imdb.num_classes, training=True)
	train_size = 0
	dataset_train = CocoDataset()
	train_size += dataset_train.load_coco(args.dataset_dir, "train", year=args.year, auto_download=args.download)
	# train_size += dataset_train.load_coco(args.dataset_dir, "valminusminival", year=args.year, auto_download=args.download)
	dataset_train.prepare()


	config = CocoConfig()

	dataloader = data_generator(dataset_train, config)

	# initilize the tensor holder here.
	im_data = torch.FloatTensor(1)
	im_info = torch.FloatTensor(1)
	num_boxes = torch.LongTensor(1)
	gt_boxes = torch.FloatTensor(1)

	##################
	# image mask
	##################

	im_mask = torch.FloatTensor(1)

	# ship to cuda
	if args.cuda:
		im_data = im_data.cuda()
		im_info = im_info.cuda()
		num_boxes = num_boxes.cuda()
		gt_boxes = gt_boxes.cuda()

		##################
		# cuda support
		##################
		im_mask = im_mask.cuda()

	# make variable
	im_data = Variable(im_data)
	im_info = Variable(im_info)
	num_boxes = Variable(num_boxes)
	gt_boxes = Variable(gt_boxes)

	##################
	# mask variable
	##################
	im_mask = Variable(im_mask)

	if args.cuda:
		cfg.CUDA = True
	'''
	# initilize the network here.
	if args.net == 'vgg16':
	  fasterRCNN = vgg16(imdb.classes, pretrained=True, class_agnostic=args.class_agnostic)
	elif args.net == 'res101':
	  fasterRCNN = resnet(imdb.classes, 101, pretrained=True, class_agnostic=args.class_agnostic)
	elif args.net == 'res50':
	  fasterRCNN = resnet(imdb.classes, 50, pretrained=True, class_agnostic=args.class_agnostic)
	elif args.net == 'res152':
	  fasterRCNN = resnet(imdb.classes, 152, pretrained=True, class_agnostic=args.class_agnostic)
	else:
	  print("network is not defined")
	  pdb.set_trace()
	'''

	print("Loading vgg16")
	n_classes = CocoConfig.NUM_CLASSES
	fasterRCNN = vgg16(n_classes, config,  pretrained=True, class_agnostic=args.class_agnostic)

	fasterRCNN.create_architecture()


	# Load pre trained model
	pre_trained_model = torch.load(pre_trained_model_path, map_location='cpu')['model']
	fixed_layers = pre_trained_model.keys()

	# These are output layers to be fine tuned
	layers_excluded = ["RCNN_rpn.RPN_cls_score.weight",
						"RCNN_rpn.RPN_cls_score.bias",
						"RCNN_rpn.RPN_bbox_pred.weight",
						"RCNN_rpn.RPN_bbox_pred.bias",
						"RCNN_cls_score.weight",
						"RCNN_cls_score.bias",
						"RCNN_bbox_pred.weight",
						"RCNN_bbox_pred.bias"
					  ]
	fixed_layers = list(set(fixed_layers) - set(layers_excluded))

	# Modify the state dict and load the parameters.

	fasterRCNN_state_dict = fasterRCNN.state_dict()
	for parameter in fasterRCNN_state_dict.keys():
		if parameter in fixed_layers:
			fasterRCNN_state_dict[parameter] = pre_trained_model[parameter]
	fasterRCNN.load_state_dict(fasterRCNN_state_dict)

	# Fix the gradient of layers which are pre trained
	for name, parameter in fasterRCNN.named_parameters():
		if name in fixed_layers:
			parameter.requires_grad = False


	lr = cfg.TRAIN.LEARNING_RATE
	lr = args.lr
	# tr_momentum = cfg.TRAIN.MOMENTUM
	# tr_momentum = args.momentum

	params = []
	for key, value in dict(fasterRCNN.named_parameters()).items():
		if value.requires_grad:
			if 'bias' in key:
				params += [{'params': [value], 'lr': lr * (cfg.TRAIN.DOUBLE_BIAS + 1), \
							'weight_decay': cfg.TRAIN.BIAS_DECAY and cfg.TRAIN.WEIGHT_DECAY or 0}]
			else:
				params += [{'params': [value], 'lr': lr, 'weight_decay': cfg.TRAIN.WEIGHT_DECAY}]

	print (fasterRCNN)
	if args.optimizer == "adam":
		lr = lr * 0.1
		optimizer = torch.optim.Adam(params)

	elif args.optimizer == "sgd":
		optimizer = torch.optim.SGD(params, momentum=cfg.TRAIN.MOMENTUM)

	if args.resume:
		load_name = os.path.join(output_dir,
								 'faster_rcnn_{}_{}_{}.pth'.format(args.checksession, args.checkepoch, args.checkpoint))
		print("loading checkpoint %s" % (load_name))
		checkpoint = torch.load(load_name)
		args.session = checkpoint['session']
		args.start_epoch = checkpoint['epoch']
		fasterRCNN.load_state_dict(checkpoint['model'])
		optimizer.load_state_dict(checkpoint['optimizer'])
		lr = optimizer.param_groups[0]['lr']
		if 'pooling_mode' in checkpoint.keys():
			cfg.POOLING_MODE = checkpoint['pooling_mode']
		# print("loaded checkpoint %s" % (load_name))

	if args.mGPUs:
		fasterRCNN = nn.DataParallel(fasterRCNN)

	if args.cuda:
		fasterRCNN.cuda()

	iters_per_epoch = int(train_size / args.batch_size)

	for epoch in range(args.start_epoch, args.max_epochs + 1):
		# setting to train mode
		fasterRCNN.train()
		loss_temp = 0
		start = time.time()

		if epoch % (args.lr_decay_step + 1) == 0:
			adjust_learning_rate(optimizer, args.lr_decay_gamma)
			lr *= args.lr_decay_gamma

		data_iter = iter(dataloader)
		for step in range(iters_per_epoch):
			try:
				_image, _image_meta, _gt_class_id, _gt_box, _gt_mask = next(data_iter)  # TODO yield image, image_meta, gt_class_ids, gt_boxes, gt_masks
			except FileNotFoundError as e:
				print (e)
				continue

			_image = np.array(_image)
			_image_meta = np.array(_image_meta)
			_gt_class_id = np.array(_gt_class_id)

			__gt_boxes = np.empty((len(_gt_class_id), 5), dtype=np.float32)
			__gt_boxes[:, 0:4] = np.array(_gt_box)
			__gt_boxes[:, 4] = _gt_class_id
			_gt_box = np.array(__gt_boxes)


			_gt_mask = np.array(_gt_mask).astype(np.float64)

			im_data = torch.from_numpy(_image)
			im_data = im_data[np.newaxis, :].permute(0, 3, 1, 2).type('torch.FloatTensor')
			im_info = torch.Tensor(_image_meta).type('torch.FloatTensor').unsqueeze_(0)
			gt_boxes = torch.Tensor(_gt_box).type('torch.FloatTensor').unsqueeze_(0)
			im_mask = torch.Tensor(_gt_mask).type('torch.FloatTensor').unsqueeze_(0)

			if args.cuda:
				im_data = im_data.cuda()
				im_info = im_info.cuda()
				gt_boxes = gt_boxes.cuda()
				im_mask = im_mask.cuda()
			#print (im_info.size())
			#print (gt_boxes.size())
			#print (im_mask.size())
			fasterRCNN.zero_grad()
			rois, cls_prob, bbox_pred, \
			rpn_loss_cls, rpn_loss_box, \
			RCNN_loss_cls, RCNN_loss_bbox, \
			rois_label, mask_pred, RCNN_mask_loss = fasterRCNN(im_data, im_info, gt_boxes, num_boxes,
															   im_mask)  # MASK LOSS

			if RCNN_loss_cls.size() and RCNN_loss_cls.size()[0] > 0:
				loss = rpn_loss_cls.mean() + rpn_loss_box.mean() \
					   + RCNN_loss_cls.mean() + RCNN_loss_bbox.mean() + RCNN_mask_loss.mean()  # ADDED MASK LOSS
			else:
				loss = rpn_loss_cls.mean() + rpn_loss_box.mean()

			loss_temp += loss.data[0]

			# backward
			optimizer.zero_grad()
			loss.backward()
			# if args.net == "vgg16":
			# 	clip_gradient(fasterRCNN, 10.)
			optimizer.step()

			if step % args.disp_interval == 0:
				end = time.time()
				if step > 0:
					loss_temp /= args.disp_interval

				if args.mGPUs:
					loss_rpn_cls = rpn_loss_cls.mean().data[0]
					loss_rpn_box = rpn_loss_box.mean().data[0]
					loss_rcnn_cls = RCNN_loss_cls.mean().data[0] if RCNN_loss_cls.size() else 0
					loss_rcnn_box = RCNN_loss_bbox.mean().data[0] if RCNN_loss_bbox.size() else 0
					loss_rcnn_mask = RCNN_mask_loss.mean().data[0] if RCNN_mask_loss.size() else 0
					fg_cnt = torch.sum(rois_label.data.ne(0))
					bg_cnt = rois_label.data.numel() - fg_cnt
				else:
					loss_rpn_cls = rpn_loss_cls.data[0]
					loss_rpn_box = rpn_loss_box.data[0]
					if RCNN_loss_cls.size() and RCNN_loss_cls.size()[0] > 0:
						loss_rcnn_cls = RCNN_loss_cls.data[0] if RCNN_loss_cls.size() else 0
						loss_rcnn_box = RCNN_loss_bbox.data[0] if RCNN_loss_bbox.size() else 0
						loss_rcnn_mask = RCNN_mask_loss.data[0] if RCNN_mask_loss.size() else 0
					else:
						loss_rcnn_cls = 0
						loss_rcnn_box = 0
						loss_rcnn_mask = 0
					fg_cnt = torch.sum(rois_label.data.ne(0))
					bg_cnt = rois_label.data.numel() - fg_cnt

				print("[session %d][epoch %2d][iter %4d/%4d] loss: %.4f, lr: %.2e" \
					  % (args.session, epoch, step, iters_per_epoch, loss_temp, lr))
				print("\t\t\tfg/bg=(%d/%d), time cost: %f" % (fg_cnt, bg_cnt, end - start))
				print("\t\t\trpn_cls: %.4f, rpn_box: %.4f, rcnn_cls: %.4f, rcnn_box %.4f, rcnn_mask %.4f" \
					  % (loss_rpn_cls, loss_rpn_box, loss_rcnn_cls, loss_rcnn_box, loss_rcnn_mask))  # MASK LOSS
				if args.use_tfboard:
					info = {
						'loss': loss_temp,
						'loss_rpn_cls': loss_rpn_cls,
						'loss_rpn_box': loss_rpn_box,
						'loss_rcnn_cls': loss_rcnn_cls,
						'loss_rcnn_box': loss_rcnn_box,
						'loss_rcnn_mask': loss_rcnn_mask  # MASK LOSS
					}
					for tag, value in info.items():
						logger.scalar_summary(tag, value, step)

				loss_temp = 0
				start = time.time()

		if args.mGPUs:
			save_name = os.path.join(output_dir, 'mask_faster_rcnn_{}_{}_{}.pth'.format(args.session, epoch, step))
			save_checkpoint({
				'session': args.session,
				'epoch': epoch + 1,
				'model': fasterRCNN.module.state_dict(),
				'optimizer': optimizer.state_dict(),
				'pooling_mode': cfg.POOLING_MODE,
				'class_agnostic': args.class_agnostic,
			}, save_name)
		else:
			save_name = os.path.join(output_dir, 'mask_faster_rcnn_{}_{}_{}.pth'.format(args.session, epoch, step))
			save_checkpoint({
				'session': args.session,
				'epoch': epoch + 1,
				'model': fasterRCNN.state_dict(),
				'optimizer': optimizer.state_dict(),
				'pooling_mode': cfg.POOLING_MODE,
				'class_agnostic': args.class_agnostic,
			}, save_name)
		print('save model: {}'.format(save_name))

		end = time.time()
		print(end - start)

